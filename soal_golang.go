package main

import (
	"encoding/json"
	"fmt"
)

type Products struct {
	ID         int
	Name       string
	Pricelists Pricelists
}

type Pricelists struct {
	ID       int
	Category string
	Brand    string
	Price    int
}

var jsonSample = `
[
  {
    "ID": 1,
    "Name": "Pena Hitam",
    "Price": 0,
    "Pricelists": {
      "ID": 1,
      "Category": "pena",
      "Brand": "bagus"
    }
  },
  {
    "ID": 2,
    "Name": "Pena Hitam",
    "Price": 0,
    "Pricelists": {
      "ID": 1,
      "Category": "pena",
      "Brand": "laris"
    }
  }
]
`

func main() {
	mapArr := getMaps()
	if bte, e := json.Marshal(mapArr); nil == e {
		fmt.Println(string(bte))
	}
}

func getMaps() map[string]map[string]string {
	var products []Products
	output := map[string]map[string]string{}

	if e := json.Unmarshal([]byte(jsonSample), &products); nil != e {
		panic(e)
	}

	for i := range products {
		productToMapItem(products[i], output)
	}

	return output
}

func productToMapItem(product Products, output map[string]map[string]string) {
	// ... ISI JAWABAN KAMU PADA FUNGSI INI

	// hasil yang diharapkan
	// output["pena"] = map[string]string{
	// 	"bagus": "Pena Hitam",
	// 	"laris": "Pena Hitam",
	// }

	if product.ID == 1 {
		output[product.Pricelists.Category] = make(map[string]string)
	}
	output[product.Pricelists.Category][product.Pricelists.Brand] = product.Name
	// output["pena"]["laris"] = "Pena Hitam"

	switch product.Pricelists.Category {
	case "pena":
		// output["pena"] = map[string]string{
		// 	product.Pricelists.Brand: product.Name,
		// 	// product.Pricelists.Brand: product.Name,
		// }
		// output["pena"] = append(output["pena"],map[string]string{
		// 	product.Pricelists.Brand: product.Name,
		// })
	}

	// fmt.Println("masok >> ", product)
	// fmt.Println("result >> ", output)
	return
}
