BEGIN TRANSACTION;

UPDATE products
SET p.price = pr.price
FROM products p, pricelists pr
WHERE
    p.category = pr.category
    AND p.brand.= pr.brand;

COMMIT;